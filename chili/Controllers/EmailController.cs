﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace chili.Controllers
{
    public class EmailController : Controller
    {
        //
        // GET: /Email/

        public JsonResult SendEmail(string name, string emailAddress, string comments)
        {
            try
            {
                StringBuilder strBody = new StringBuilder();
                strBody.AppendLine(string.Format("Name: {0}<br/>", name));
                strBody.AppendLine(string.Format("Email: <a mailto:'{0}'>{0}</a><br/>", emailAddress));
                strBody.AppendLine(string.Format("Comments: {0}<br/>", comments));
                string email = "charles.catron@gmail.com";
                string password = "05ella06";

                var loginInfo = new NetworkCredential(email, password);
                var msg = new MailMessage();
                var smtpClient = new SmtpClient("smtp.gmail.com", 587);

                msg.From = new MailAddress(email);
                msg.To.Add(new MailAddress(emailAddress));
                msg.Subject = "chili! contact form";
                msg.Body = strBody.ToString();
                msg.IsBodyHtml = true;

                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = loginInfo;
                smtpClient.Send(msg);
            }
            catch (Exception ex)
            {
                return Json(ex, "401", JsonRequestBehavior.AllowGet);
            }

            return Json("message sent", JsonRequestBehavior.AllowGet);
        }

    }
}
